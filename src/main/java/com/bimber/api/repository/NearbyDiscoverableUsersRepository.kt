package com.bimber.api.repository

import com.bimber.api.model.network.NearbyDiscoverableUser
import com.bimber.api.model.network.NearbyDiscoverableUsers
import com.bimber.api.model.network.NearbyDiscoverableUsersWithLocations
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.util.HashMap

import javax.sql.DataSource

@Repository
open class NearbyDiscoverableUsersRepository(dataSource: DataSource) {

    private val namedTemplate: NamedParameterJdbcTemplate

    init {
        namedTemplate = NamedParameterJdbcTemplate(dataSource)
    }

    open fun getNearbyDiscoverableUsers(uId: String, latitude: Double, longitude: Double, range: Int): NearbyDiscoverableUsers {
        val nearbyMap = HashMap<String, Any>()
        nearbyMap.put("uId", uId)
        nearbyMap.put("latitude", latitude)
        nearbyMap.put("longitude", longitude)
        nearbyMap.put("user_range", range * 1000)
        return NearbyDiscoverableUsers(namedTemplate.queryForList(NEARBY_DISCOVERABLE_USERS_QUERY, nearbyMap, String::class.java))
    }

    open fun getNearbyDiscoverableUsersWithLocation(uId: String, latitude: Double, longitude: Double, range: Int): NearbyDiscoverableUsersWithLocations {
        val nearbyMap = HashMap<String, Any>()
        nearbyMap.put("uId", uId)
        nearbyMap.put("latitude", latitude)
        nearbyMap.put("longitude", longitude)
        nearbyMap.put("user_range", range * 1000)
        val query = namedTemplate.query(NEARBY_DISCOVERABLE_USERS_WITH_LOCATIONS_QUERY, nearbyMap,
                { rm, _ ->
                    NearbyDiscoverableUser(rm.getString("location_uid"),
                            rm.getString("location_name"),
                            rm.getFloat("distance"))
                })
        return NearbyDiscoverableUsersWithLocations(query)
    }

    companion object {
        private const val NEARBY_DISCOVERABLE_USERS_WITH_LOCATIONS_QUERY = """
            SELECT location_uid, location_name, geolocation,
            ST_Distance(
            user_locations.geolocation,
            ST_SetSRID(ST_MakePoint(:latitude, :longitude), 4326)) / 1000.0 as distance
            FROM user_locations
            LEFT JOIN evaluations ON evaluated_uid = location_uid AND evaluating_uid = :uId
            WHERE evaluated_uid IS NULL AND ST_DISTANCE(geolocation,ST_SetSRID(ST_MakePoint(:latitude, :longitude), 4326)) < :user_range AND location_uid<>:uId
            ORDER BY user_locations.last_present_timestamp DESC LIMIT 50
            """

        private const val NEARBY_DISCOVERABLE_USERS_QUERY = """
            SELECT location_uid
            FROM user_locations
            LEFT JOIN evaluations ON evaluated_uid = location_uid AND evaluating_uid = :uId
            WHERE evaluated_uid IS NULL AND ST_DISTANCE(geolocation,ST_SetSRID(ST_MakePoint(:latitude, :longitude), 4326)) < :user_range AND location_uid<>:uId
            ORDER BY user_locations.last_present_timestamp DESC LIMIT 50
            """
    }
}
