package com.bimber.api.repository

import com.bimber.api.model.network.UserLocationRequest
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository

import javax.sql.DataSource
import java.util.HashMap

@Repository
class LocationRepository(dataSource: DataSource) {

    private val namedTemplate: NamedParameterJdbcTemplate

    init {
        this.namedTemplate = NamedParameterJdbcTemplate(dataSource)
    }

    fun insertLocation(uId: String, userLocationRequest: UserLocationRequest) {
        val evaluationParams = HashMap<String, Any>()
        evaluationParams["location_uid"] = uId
        evaluationParams["latitude"] = userLocationRequest.latitude
        evaluationParams["longitude"] = userLocationRequest.longitude
        evaluationParams["range"] = userLocationRequest.range
        evaluationParams["type"] = userLocationRequest.type.toString()
        evaluationParams["location_name"] = userLocationRequest.locationName
        namedTemplate.update(INSERT_USER_LOCATION, evaluationParams)
    }

    fun updateLocationTimestamp(uId: String) {
        val evaluationParams = HashMap<String, Any>()
        evaluationParams["location_uid"] = uId
        namedTemplate.update(UPDATE_USER_LOCATION_TIMESTAMP, evaluationParams)
    }

    fun removeLocation(uId: String) {
        val removeParams = HashMap<String, Any>()
        removeParams["location_uid"] = uId
        namedTemplate.update(REMOVE_USER_LOCATION, removeParams)
    }

    companion object {

        private const val INSERT_USER_LOCATION = """
            INSERT INTO user_locations (location_uid, latitude, longitude, range, type, location_name, geolocation, last_present_timestamp)
            VALUES(:location_uid, :latitude, :longitude, :range, :type, :location_name, ST_SetSRID(ST_MakePoint(:latitude, :longitude), 4326), now())
            ON CONFLICT (location_uid) DO UPDATE SET latitude = excluded.latitude, longitude = excluded.longitude, range = excluded.range, type = excluded.type, location_name = excluded.location_name, geolocation = ST_SetSRID(ST_MakePoint(excluded.latitude::numeric, excluded.longitude::numeric), 4326), last_present_timestamp = now()
            """

        private const val UPDATE_USER_LOCATION_TIMESTAMP = """
            UPDATE user_locations
            SET last_present_timestamp = now()
            WHERE location_uid = :location_uid
            """

        private const val REMOVE_USER_LOCATION = "DELETE FROM user_locations WHERE location_uid = :location_uid"
    }

}
