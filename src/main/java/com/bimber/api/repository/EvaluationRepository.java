package com.bimber.api.repository;

import com.bimber.api.model.persistence.EvaluationStatus;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Repository
public class EvaluationRepository {
    private static final String INSERT_EVALUATION_STATUS = "INSERT INTO evaluations (evaluating_uid, evaluated_uid, status) VALUES(:evaluating_uid, :evaluated_uid, :status) ON CONFLICT (evaluating_uid, evaluated_uid) DO UPDATE SET status = excluded.status";
    private static final String REMOVE_EVALUATION_STATUS = "DELETE FROM evaluations WHERE evaluating_uid=:evaluating_uid AND evaluated_uid=:evaluated_uid";
    private static final String REMOVE_EVALUATION_STATUSES = "DELETE FROM evaluations WHERE evaluating_uid=:evaluating_uid";


    private final NamedParameterJdbcTemplate namedTemplate;

    public EvaluationRepository(DataSource dataSource) {
        this.namedTemplate = new NamedParameterJdbcTemplate(dataSource);
    }


    public void insertEvaluation(String evaluatingUId, String evaluatedUId, EvaluationStatus evaluation) {
        Map<String, Object> evaluationParams = new HashMap<>();
        evaluationParams.put("evaluating_uid", evaluatingUId);
        evaluationParams.put("evaluated_uid", evaluatedUId);
        evaluationParams.put("status", evaluation.toString());
        namedTemplate.update(INSERT_EVALUATION_STATUS, evaluationParams);
    }

    public void removeEvaluation(String evaluatingUId, String evaluatedUId) {
        Map<String, Object> evaluationParams = new HashMap<>();
        evaluationParams.put("evaluating_uid", evaluatingUId);
        evaluationParams.put("evaluated_uid", evaluatedUId);
        namedTemplate.update(REMOVE_EVALUATION_STATUS, evaluationParams);
    }

    public void removeEvaluations(String evaluatingUId) {
        Map<String, Object> evaluationParams = new HashMap<>();
        evaluationParams.put("evaluating_uid", evaluatingUId);
        namedTemplate.update(REMOVE_EVALUATION_STATUSES, evaluationParams);
    }

    public String insertBatchedEvaluations(String evaluatingUId, Map<String, EvaluationStatus> userEvaluations) {
        if (userEvaluations == null) {
            return "BAD";
        }
        for (Map.Entry<String, EvaluationStatus> evaluationStatusEntry : userEvaluations.entrySet()) {
            Map<String, String> params = new HashMap<>();
            params.put("evaluating_uid", evaluatingUId);
            params.put("evaluated_uid", evaluationStatusEntry.getKey());
            params.put("status", evaluationStatusEntry.getValue().toString());
            namedTemplate.update(INSERT_EVALUATION_STATUS, params);
        }
        return "OK";
    }

//    public void checkMatchStatus(String evaluatingUId, String evaluatedUId) {
//        Map<String, Object> evaluationParams = new HashMap<>();
//        evaluationParams.put("evaluatingUId", evaluatingUId);
//        evaluationParams.put("evaluatedUId", evaluatedUId);
//        namedTemplate.query("SELECT * FROM evaluations as evaluatingUIdEvaluations WHERE  evaluatingUId = :evaluatingUId AND evaluatedUId = :evaluatedUId",
//                evaluationParams);
//    }

}
