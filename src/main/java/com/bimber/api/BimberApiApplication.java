package com.bimber.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BimberApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BimberApiApplication.class, args);
    }
}
