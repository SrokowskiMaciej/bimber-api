package com.bimber.api.controller

import com.bimber.api.model.network.NearbyDiscoverableUser
import com.bimber.api.model.network.NearbyDiscoverableUsers
import com.bimber.api.model.network.NearbyDiscoverableUsersWithLocations
import com.bimber.api.repository.NearbyDiscoverableUsersRepository
import com.fasterxml.jackson.annotation.JsonView
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import sun.reflect.generics.reflectiveObjects.NotImplementedException

@RestController
@RequestMapping("nearbyDiscoverableUsers")
open class NearbyDiscoverableUsersController {

    @Autowired
    internal var repository: NearbyDiscoverableUsersRepository? = null

    @RequestMapping(value = ["/{uId}/location"],
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE))
    fun nearbyDiscoverableUsers(@PathVariable(name = "uId") uId: String): String {
        throw NotImplementedException()
    }

    @RequestMapping(value = ["/{uId}"],
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE), params = arrayOf("latitude", "longitude", "range"))
    fun nearbyDiscoverableUsersAt(@PathVariable(name = "uId") uId: String,
                                  @RequestParam(value = "latitude") latitude: Double,
                                  @RequestParam(value = "longitude") longitude: Double,
                                  @RequestParam(value = "range") range: Int): NearbyDiscoverableUsers {
        return repository!!.getNearbyDiscoverableUsers(uId, latitude, longitude, range)
    }

    @RequestMapping(value = ["/{uId}/withLocations"],
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_UTF8_VALUE), params = arrayOf("latitude", "longitude", "range"))
    fun nearbyDiscoverableUsersWithLocations(@PathVariable(name = "uId") uId: String,
                                             @RequestParam(value = "latitude") latitude: Double,
                                             @RequestParam(value = "longitude") longitude: Double,
                                             @RequestParam(value = "range") range: Int): List<NearbyDiscoverableUser> {
        return repository!!.getNearbyDiscoverableUsersWithLocation(uId, latitude, longitude, range).discoverableUsers
    }
}
