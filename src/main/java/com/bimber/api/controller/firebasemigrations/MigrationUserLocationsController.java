package com.bimber.api.controller.firebasemigrations;

import com.bimber.api.model.network.UserLocationRequest;
import com.bimber.api.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("migration/usersLocations")
public class MigrationUserLocationsController {

    @Autowired
    LocationRepository locationRepository;

    @RequestMapping(value = "/{uId}",
            method = RequestMethod.PUT)
    public String setUserLocation(@PathVariable(name = "uId") String uId,
                                  @Valid @RequestBody UserLocationRequest userLocationRequest) {
        locationRepository.insertLocation(uId, userLocationRequest);
        return "OK";
    }

    @RequestMapping(value = "/{uId}",
            method = RequestMethod.DELETE)
    public String removeUserLocation(@PathVariable(name = "uId") String uId) {
        locationRepository.removeLocation(uId);
        return "OK";
    }
}
