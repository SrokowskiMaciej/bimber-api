package com.bimber.api.controller.firebasemigrations;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("firebase.migration")
public class FirebaseMigrationProperties {

    private String secret;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
