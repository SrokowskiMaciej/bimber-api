package com.bimber.api.controller.firebasemigrations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class FirebaseMigrationInterceptor implements HandlerInterceptor {

    private final FirebaseMigrationProperties firebaseMigrationProperties;

    @Autowired
    public FirebaseMigrationInterceptor(FirebaseMigrationProperties firebaseMigrationProperties) {
        this.firebaseMigrationProperties = firebaseMigrationProperties;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String authorization = request.getHeader("Authorization");
        if (authorization != null && authorization.equals(firebaseMigrationProperties.getSecret())) {
            return true;
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        }
    }
}
