package com.bimber.api.controller.firebasemigrations;

import com.bimber.api.model.network.EvaluationRequest;
import com.bimber.api.model.persistence.EvaluationStatus;
import com.bimber.api.repository.EvaluationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("migration/evaluation")
public class MigrationUserEvaluationsController {

    @Autowired
    EvaluationRepository evaluationRepository;


    @RequestMapping(value = "/{evaluatingUId}/{evaluatedUId}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String putEvaluation(@PathVariable(name = "evaluatingUId") String evaluatingUId,
                                 @PathVariable(name = "evaluatedUId") String evaluatedUId,
                                 @Valid @RequestBody EvaluationRequest evaluationRequest) {
        evaluationRepository.insertEvaluation(evaluatingUId, evaluatedUId, evaluationRequest.getStatus());
        return "OK";
    }

    @RequestMapping(value = "/{evaluatingUId}/{evaluatedUId}",
            method = RequestMethod.DELETE)
    public String deleteEvaluation(@PathVariable(name = "evaluatingUId") String evaluatingUId,
                                 @PathVariable(name = "evaluatedUId") String evaluatedUId) {
        evaluationRepository.removeEvaluation(evaluatingUId, evaluatedUId);
        return "OK";
    }

    @RequestMapping(value = "/{evaluatingUId}",
            method = RequestMethod.DELETE)
    public String deleteEvaluations(@PathVariable(name = "evaluatingUId") String evaluatingUId) {
        evaluationRepository.removeEvaluations(evaluatingUId);
        return "OK";
    }

    @RequestMapping(value = "/batch/{evaluatingUId}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String batchEvaluation(@PathVariable(name = "evaluatingUId") String evaluatingUId,
                                  @Valid @RequestBody Map<String,EvaluationStatus> userEvaluationsMap){
        return evaluationRepository.insertBatchedEvaluations(evaluatingUId, userEvaluationsMap);
    }
}
