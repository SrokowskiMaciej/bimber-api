package com.bimber.api.controller.cron;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutures;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
public class GroupDiscoveryController {

    @Autowired
    PubSubProjectIdProperty pubSubProjectIdProperty;
    @RequestMapping(value = "/publish/group-time-check-tick", method = RequestMethod.GET)
    public String publishGroupDiscoverabilityCheck() throws Exception {

        ProjectTopicName topicName = ProjectTopicName.of(pubSubProjectIdProperty.getId(), "group-time-check-tick");
        // Create a publisher instance with default settings bound to the topic
        Publisher publisher = null;
        try {
            publisher = Publisher.newBuilder(topicName).build();
            ByteString data = ByteString.copyFromUtf8("tick");
            PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

            // Once published, returns a server-assigned message id (unique within the topic)
            ApiFuture<List<String>> future = ApiFutures.allAsList(Collections.singletonList(publisher.publish(pubsubMessage)));

            future.get();
        } catch (IOException | ExecutionException | InterruptedException e) {
            throw e;
        } finally {
            if (publisher != null) {
                publisher.shutdown();
                // When finished with the publisher, shutdown to free up resources.
            }
        }
        return "OK";
    }
}
