package com.bimber.api.controller.cron;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("pubsub.project")
public class PubSubProjectIdProperty {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
