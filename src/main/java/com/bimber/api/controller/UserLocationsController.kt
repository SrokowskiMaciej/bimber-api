package com.bimber.api.controller

import com.bimber.api.model.network.UserLocationRequest
import com.bimber.api.repository.LocationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

@RestController
@CrossOrigin(exposedHeaders = arrayOf("errors, content-type"))
@RequestMapping("usersLocations")
open class UserLocationsController {

    @Autowired
    internal var locationRepository: LocationRepository? = null

    @RequestMapping(value = "/{uId}/timestamp", method = arrayOf(RequestMethod.PUT))
    fun updateLocationTimestamp(@PathVariable(name = "uId") uId: String): String {
        locationRepository!!.updateLocationTimestamp(uId)
        return "OK"
    }
}
