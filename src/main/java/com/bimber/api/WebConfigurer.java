package com.bimber.api;

import com.bimber.api.controller.cron.PubSubProjectIdProperty;
import com.bimber.api.controller.firebasemigrations.FirebaseMigrationInterceptor;
import com.bimber.api.controller.firebasemigrations.FirebaseMigrationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableConfigurationProperties({FirebaseMigrationProperties.class, PubSubProjectIdProperty.class})
public class WebConfigurer implements WebMvcConfigurer {

    @Autowired
    FirebaseMigrationInterceptor firebaseMigrationInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(firebaseMigrationInterceptor).addPathPatterns("/migration/**");
    }
}
