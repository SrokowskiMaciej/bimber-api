package com.bimber.api.model.persistence;

import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class MatchStatus {

    @NotNull
    private final Map<String, EvaluationStatus> evaluations;
    @Nullable
    private final String dialogId;

    public MatchStatus(@NotNull Map<String, EvaluationStatus> evaluations, @Nullable String dialogId) {
        this.evaluations = evaluations;
        this.dialogId = dialogId;
    }
}
