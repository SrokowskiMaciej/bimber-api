package com.bimber.api.model.persistence;

public enum EvaluationStatus {
    LIKED,
    DISLIKED
}
