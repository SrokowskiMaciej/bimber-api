package com.bimber.api.model.persistence;

public enum LocationType {
    CURRENT_LOCATION,
    CUSTOM_LOCATION
}
