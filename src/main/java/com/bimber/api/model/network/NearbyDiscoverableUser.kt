package com.bimber.api.model.network

class NearbyDiscoverableUser(val uId: String,
                             val locationName: String,
                             val distance: Float)
