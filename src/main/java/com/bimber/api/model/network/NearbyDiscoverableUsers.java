package com.bimber.api.model.network;

import java.util.List;

public class NearbyDiscoverableUsers {

    private   List<String> discoverableUsers;

    public NearbyDiscoverableUsers() {
    }

    public NearbyDiscoverableUsers(List<String> discoverableUsers) {
        this.discoverableUsers = discoverableUsers;
    }

    public List<String> getDiscoverableUsers() {
        return discoverableUsers;
    }

    public void setDiscoverableUsers(List<String> discoverableUsers) {
        this.discoverableUsers = discoverableUsers;
    }
}
