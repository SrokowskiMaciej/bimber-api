package com.bimber.api.model.network;

import com.bimber.api.model.persistence.LocationType;

import javax.validation.constraints.*;

public class UserLocationRequest {

    @NotNull(message = "Location locationName can't be null")
    private String locationName;
    @DecimalMin("-90.0")
    @DecimalMax("90.0")
    private double latitude;
    @DecimalMin("-180.0")
    @DecimalMax("180.0")
    private double longitude;
    @Min(1)
    @Max(100)
    private int range;
    @NotNull(message = "Location type can't be null")
    private LocationType type;

    public UserLocationRequest() {
    }

    public UserLocationRequest(String locationName, double latitude, double longitude, int range, LocationType type) {
        this.locationName = locationName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.range = range;
        this.type = type;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public LocationType getType() {
        return type;
    }

    public void setType(LocationType type) {
        this.type = type;
    }
}
