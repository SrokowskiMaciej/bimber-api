package com.bimber.api.model.network;


import com.bimber.api.model.persistence.EvaluationStatus;

import javax.validation.constraints.NotNull;

public class EvaluationRequest {

    @NotNull(message = "Evaluation status can't be null")
    private EvaluationStatus status;

    public EvaluationRequest() {
        super();
    }

    public EvaluationRequest(EvaluationStatus status) {
        this.status = status;
    }

    public EvaluationStatus getStatus() {
        return status;
    }

    public void setStatus(EvaluationStatus status) {
        this.status = status;
    }
}


