package com.bimber.api.model.network;

import java.util.List;

public class NearbyDiscoverableUsersWithLocations {

    private List<NearbyDiscoverableUser> discoverableUsers;

    public NearbyDiscoverableUsersWithLocations() {
    }

    public NearbyDiscoverableUsersWithLocations(List<NearbyDiscoverableUser> discoverableUsers) {
        this.discoverableUsers = discoverableUsers;
    }

    public List<NearbyDiscoverableUser> getDiscoverableUsers() {
        return discoverableUsers;
    }

    public void setDiscoverableUsers(List<NearbyDiscoverableUser> discoverableUsers) {
        this.discoverableUsers = discoverableUsers;
    }
}
