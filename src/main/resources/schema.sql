CREATE TABLE IF NOT EXISTS evaluations (
  evaluating_uid TEXT,
  evaluated_uid  TEXT,
  status        TEXT,
  PRIMARY KEY (evaluating_uid, evaluated_uid)
);

CREATE TABLE IF NOT EXISTS user_locations (
  location_uid TEXT PRIMARY KEY,
  latitude    TEXT,
  longitude   TEXT,
  range INTEGER,
  type TEXT,
  location_name TEXT,
  last_present_timestamp TIMESTAMP NOT NULL,
  geolocation geography(POINT)
);